/**
 @author Ben Cardoen.
 */

#include <iostream>
#include <vector>
#include <list>
#include <array>
#include <queue>
#include <algorithm>
#include <random>
#include "schedulerfactory.h"
#include <thread>
#include <atomic>
#include <boost/heap/fibonacci_heap.hpp>
#include "gtest/gtest.h"

using std::cout;
using std::endl;
using std::atomic;
using std::thread;
using namespace N_Scheduler;
typedef ExampleItem T_TypeUsed;

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

